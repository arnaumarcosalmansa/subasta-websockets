package hello;

import java.security.Principal;

//STOMP --> Simple Text-Oriented Message Protocol
public class StompPrincipal implements Principal {
	
	private String name = null;
	
	public StompPrincipal(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.name;
	}

}
