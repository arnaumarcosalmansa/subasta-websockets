package hello;

public class NoHaySubastaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoHaySubastaException(String string) {
		super(string);
	}
	
}
