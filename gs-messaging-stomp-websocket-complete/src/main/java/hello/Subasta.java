package hello;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Subasta {
	private String producto = "Producto ";
	private Date inicio = new Date();
	private List<Puja> pujas = new ArrayList<Puja>();

	public Subasta(String producto, Date inicio, List<Puja> pujas) {
		super();
		this.producto = producto;
		this.inicio = inicio;
		this.pujas = pujas;
	}

	public Subasta() {
		super();
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public List<Puja> getPujas() {
		return pujas;
	}

	public void setPujas(List<Puja> pujas) {
		this.pujas = pujas;
	}

	public Date ultimaModificacion() {
		if (pujas.isEmpty()) {
			return inicio;
		} else {
			return pujas.get(pujas.size() - 1).getMomento();
		}
	}
}
