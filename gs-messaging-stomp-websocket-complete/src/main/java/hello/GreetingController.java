package hello;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.HtmlUtils;

@EnableScheduling
@Controller
@ControllerAdvice
public class GreetingController {

	/*
	 * No me atrevo a cambiar el nombre de la clase porque el Spring se puede
	 * desconfigurar
	 */

	public static Subasta subasta = null;
	public static Semaphore mutexSubasta = new Semaphore(1, true);
	public static Date subastaCerradaEn = new Date();

	@Autowired
	private SimpMessagingTemplate template;

	@Scheduled(fixedRate = 500)
	public void greeting() throws Exception {
		long now = new Date().getTime();
		mutexSubasta.acquire();
		if (subastaCerradaEn == null) {
			subastaCerradaEn = new Date();
		}
		if (subasta == null && now - subastaCerradaEn.getTime() > 10000) {
			subasta = new Subasta();
			this.template.convertAndSend("/answer/start", "Subastando " + subasta.getProducto());
		} else if (subasta != null) {
			if (now - subasta.ultimaModificacion().getTime() > 15000) {
				if (!subasta.getPujas().isEmpty()) {
					Puja ganadora = subasta.getPujas().get(subasta.getPujas().size() - 1);
					System.out.println(ganadora.getPersona() + " " + ganadora.getSessionId());

					this.template.convertAndSend("/answer/final", "Se acabó la subasta! Gana " + ganadora.getPersona());

					this.template.convertAndSendToUser(ganadora.getSessionId(), "/answer/final",
							"Has ganado la subasta!");
				} else {
					System.out.println("Sin pujas.");
					this.template.convertAndSend("/answer/final", "La subasta termina sin pujas!");
				}
				subasta = null;
				subastaCerradaEn = new Date();
			}
		}
		mutexSubasta.release();
	}

	// funcion que no se usa pero se mantiene como ejemplo
	/*
	 * @MessageMapping("/hello")
	 * 
	 * @SendTo("/answer/puja") public Greeting greeting(HelloMessage message) throws
	 * Exception { Thread.sleep(1000); // simulated delay
	 * System.out.println("Hello, " + message.getName() + "!"); return new
	 * Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!"); }
	 */

	@ResponseBody
	@MessageMapping("/fullupdate")
	@SendToUser("/answer/update")
	public Iterable<Puja> fullupdate(Principal principal) throws Exception {
		mutexSubasta.acquire();
		if (subasta == null) {
			mutexSubasta.release();
			throw new NoHaySubastaException("No hay subasta!");
		}
		this.template.convertAndSendToUser(principal.getName(), "/user/answer/start",
				"Subastando " + subasta.getProducto());
		List<Puja> copy = new ArrayList<Puja>(subasta.getPujas());
		mutexSubasta.release();
		return copy;
	}

	@MessageMapping("/puja")
	@SendTo("/answer/puja")
	public Puja puja(SimpMessageHeaderAccessor headerAccessor, Principal principal, Puja puja) throws Exception {
		// Thread.sleep(1000); // simulated delay
		System.out.println(puja.getPersona() + " puja " + puja.getCantidad() + " el " + puja.getMomento());
		mutexSubasta.acquire();
		if (subasta == null) {
			mutexSubasta.release();
			throw new NoHaySubastaException("No hay subasta!");
		}

		if (subasta.getPujas().isEmpty()
				|| puja.getCantidad() > subasta.getPujas().get(subasta.getPujas().size() - 1).getCantidad()) {

			puja.setId(subasta.getPujas().size());
			puja.setSessionId(principal.getName());
			puja.setMomento(new Date());
			subasta.getPujas().add(puja);
		} else {
			mutexSubasta.release();
			throw new PujaDemasiadoBajaException("La puja es demasiado baja.");
		}
		mutexSubasta.release();
		return puja;
	}

	@ExceptionHandler({ PujaDemasiadoBajaException.class })
	@MessageExceptionHandler({ PujaDemasiadoBajaException.class })
	@SendToUser("/answer/error")
	public String handlePujaException(Throwable exception) {
		return exception.getMessage();
	}

	@ExceptionHandler({ NoHaySubastaException.class })
	@MessageExceptionHandler({ NoHaySubastaException.class })
	@SendToUser("/answer/error")
	public String handleNoPujaException(Throwable exception) {
		return exception.getMessage();
	}

	@ExceptionHandler
	@MessageExceptionHandler
	@SendToUser("/answer/error")
	public String handleException(Throwable exception) {
		return "Shit man.";
	}
}
