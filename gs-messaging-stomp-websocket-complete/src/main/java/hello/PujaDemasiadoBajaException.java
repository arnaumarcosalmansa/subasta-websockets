package hello;

public class PujaDemasiadoBajaException extends RuntimeException {

	public PujaDemasiadoBajaException(String string) {
		
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
